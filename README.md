**Get COFAZ**

Este script pega os dados das tabelas referentes aos ATOS COTEPE/PMPF, 
separando-as em planilhas .xlsx por quinzena/ano.

Site base:
https://www.confaz.fazenda.gov.br/legislacao/atos-pmpf