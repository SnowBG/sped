import requests
from bs4 import BeautifulSoup as bs
from datetime import datetime as dt
import pandas as pd

class atos_pmpf():
    
    def pega_links_por_ano(self):
        page = requests.get('https://www.confaz.fazenda.gov.br/legislacao/atos-pmpf', verify=False)
        sp = bs(page.content, 'html.parser')
        links=[]
        for link in sp.find_all('a'):
            links.append(link.get('href'))
            links = [l for l in links if l is not None]
        links_anos = [links[l] for l, lk in enumerate(links) if 'atos-pmpf/201' in lk]
        return links_anos


    def pega_links_dos_atos(self):
        links = self.pega_links_por_ano()
        links_at=[]
        for link in links:
            page = requests.get(link, verify=False)
            sp = bs(page.content, 'html.parser')
            for link in sp.find_all('a'):
                links_at.append(link.get('href'))
                links_at = [l for l in links_at if l is not None]
        links_atos = [links_at[l] for l, lk in enumerate(links_at) if '/pmpf0' in lk]
        return links_atos


    def pega_tabela(self):
        links = self.pega_links_dos_atos()
        for link in links:
            table = ""
            now = dt.now()
            micro = str(now.microsecond)
            page = requests.get(link, verify=False)
            quinzena = int(link[-6:-3])%2
            print(quinzena)
            print(link[-6:-3])
            if int(link[-6:-3])>1:
                if int(link[-6:-3])%2:
                    mes = int(int(link[-6:-3])/2)
                elif int(link[-6:-3])%2 == 0:
                    mes = int(int(link[-6:-3])/2)+1
            else:
                mes = 1
            ano = link[-2:]
            if quinzena:
                fortnight = 'q2'
            else:
                fortnight = 'q1'
            sp = bs(page.content, 'html.parser')
            for tb in sp.find_all('table'):
                table=table+str(tb)
            self.grava_arquivo("", f'tabela_pmpf_{mes}_{ano}_{fortnight}', '.html', str(table),f'{mes}/{ano}')
        return table

    
    def grava_arquivo(self, path, file_name, extention, data, mes_ano):
        dados = data
        df = pd.read_html(dados,thousands = '.', decimal=',')
        df_p = pd.DataFrame(df[0])
        df_p.drop([0],inplace=True)
        df_p.insert(loc=len(df_p.columns),column=df_p.shape[1],value=mes_ano)
        df_p.to_excel(path+file_name+".xlsx", sheet_name = "Atos PMPF", float_format="%.4f", header=0)

a = atos_pmpf()
a.pega_tabela()

    
    
